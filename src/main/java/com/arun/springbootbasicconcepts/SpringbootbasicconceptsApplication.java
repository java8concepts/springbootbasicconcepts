package com.arun.springbootbasicconcepts;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootbasicconceptsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootbasicconceptsApplication.class, args);
	}

}
